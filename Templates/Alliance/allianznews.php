<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head>
	<title>Travian comx</title>
	<meta http-equiv="cache-control" content="max-age=0">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="imagetoolbar" content="no">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<script src="allianznews_files/mt-full.js" type="text/javascript"></script>
	<script src="allianznews_files/unx.js" type="text/javascript"></script>
	<script src="allianznews_files/new.js" type="text/javascript"></script>
	        <link href="allianznews_files/compact.css" rel="stylesheet" type="text/css"> 
<link href="allianznews_files/lang_002.css" rel="stylesheet" type="text/css"> 
<link href="allianznews_files/travian.css" rel="stylesheet" type="text/css"> 
<link href="allianznews_files/lang.css" rel="stylesheet" type="text/css"> 
        	<script type="text/javascript">
		window.addEvent('domready', start);
	</script>
</head>


<body class="v35 gecko">


<div class="wrapper">
<img src="allianznews_files/x.gif" id="msfilter" alt="">
<div id="dynamic_header">
	</div>


<div id="header">
	<div id="mtop">
		<a href="http://tcx3.travian.com/dorf1.php" id="n1" accesskey="1"><img src="allianznews_files/x.gif" title="Village overview" alt="Village overview"></a>
		<a href="http://tcx3.travian.com/dorf2.php" id="n2" accesskey="2"><img src="allianznews_files/x.gif" title="Village centre" alt="Village centre"></a>
		<a href="http://tcx3.travian.com/karte.php" id="n3" accesskey="3"><img src="allianznews_files/x.gif" title="Map" alt="Map"></a>
		<a href="http://tcx3.travian.com/statistiken.php" id="n4" accesskey="4"><img src="allianznews_files/x.gif" title="Statistics" alt="Statistics"></a>
  		<div id="n5" class="i3">
			<a class="reports" href="http://tcx3.travian.com/berichte.php" accesskey="5"><img src="allianznews_files/x.gif" class="l" title="Reports" alt="Reports"></a>
			<a class="messages" href="http://tcx3.travian.com/nachrichten.php" accesskey="6"><img src="allianznews_files/x.gif" class="r" title="Messages" alt="Messages"></a>
		</div>
		<a href="http://tcx3.travian.com/plus.php" id="plus"><span class="plus_text"><span class="plus_g">P</span><span class="plus_o">l</span><span class="plus_g">u</span><span class="plus_o">s</span></span><img src="allianznews_files/x.gif" id="btn_plus" class="inactive" title="Plus menu" alt="Plus menu"></a>
		<div class="clear"></div>
	</div>
</div>

<div id="mid">
		<div id="side_navi">
		<a id="logo" href="http://www.travian.com/"><img src="allianznews_files/x.gif" alt="Travian"></a>
			<p>
			<a href="http://www.travian.com/">Homepage</a>
            <a href="#" onclick="return Popup(0,0);">Instructions</a>
			<a href="http://tcx3.travian.com/spieler.php?uid=7942">Profile</a>
			<a href="http://tcx3.travian.com/logout.php">Logout</a>
		</p>
		<p>
							<a href="http://forum.travian.com/" target="_blank">Forum</a>
						
					</p>
		<p>
			<a href="http://tcx3.travian.com/plus.php?id=3">Travian <b><span class="plus_g">P</span><span class="plus_o">l</span><span class="plus_g">u</span><span class="plus_o">s</span></b></a>
			<a href="http://tcx3.travian.com/support.php">Support</a>
		</p>
							</div>

	<div id="content" class="alliance">
<h1>LiQ-G - LiQuiDaTion-Green Team</h1>
<div id="textmenu">
   <a href="http://tcx3.travian.com/allianz.php">Overview</a>
 | <a href="http://tcx3.travian.com/allianz.php?s=2">Forum</a>
 | <a href="http://tcx3.travian.com/allianz.php?s=6">Chat</a>
 | <a href="http://tcx3.travian.com/allianz.php?s=3">Attacks</a>
 | <a href="http://tcx3.travian.com/allianz.php?s=4" class="selected ">News</a>
 | <a href="http://tcx3.travian.com/allianz.php?s=5">Options</a>
</div>
<table id="events" cellpadding="1" cellspacing="1"><thead>
<tr><th colspan="2">Alliance events</th></tr>
<tr>
<td>Event</td>
<td>Date</td>
</tr></thead><tbody><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a> accepted the NAP with <a href="http://tcx3.travian.com/allianz.php?aid=4171">LiQ-WW</a>.</td><td class="dat">05.09.11 16:01</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=4171">LiQ-WW</a> has offered a NAP to <a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a>.</td><td class="dat">05.09.11 15:59</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=4171">LiQ-WW</a> canceled the alliance with <a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a>.</td><td class="dat">05.09.11 15:59</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/spieler.php?uid=13867">vaqsu</a> has left the alliance.</td><td class="dat">05.09.11 05:14</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/spieler.php?uid=2721">davedab</a> has left the alliance.</td><td class="dat">03.09.11 23:34</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=4203">LIQ-C</a> has offered a confederation to <a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a>.</td><td class="dat">02.09.11 17:46</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/spieler.php?uid=4161">Bomber Dude</a> has left the alliance.</td><td class="dat">02.09.11 03:19</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a> and <a href="http://tcx3.travian.com/allianz.php?aid=68">Fate™</a> joined a confederation.</td><td class="dat">02.09.11 03:08</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=68">Fate™</a> has offered a confederation to <a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a>.</td><td class="dat">01.09.11 08:08</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a> and <a href="http://tcx3.travian.com/allianz.php?aid=4194">MERCS</a> joined a confederation.</td><td class="dat">31.08.11 16:34</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=4194">MERCS</a> has offered a confederation to <a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a>.</td><td class="dat">31.08.11 16:33</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a> canceled the NAP with <a href="http://tcx3.travian.com/allianz.php?aid=80">ABC-D.B.</a>.</td><td class="dat">29.08.11 21:44</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a> accepted the NAP with <a href="http://tcx3.travian.com/allianz.php?aid=80">ABC-D.B.</a>.</td><td class="dat">29.08.11 18:27</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=80">ABC-D.B.</a> has offered a NAP to <a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a>.</td><td class="dat">29.08.11 16:23</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/allianz.php?aid=80">ABC-D.B.</a> canceled the NAP with <a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a>.</td><td class="dat">29.08.11 15:32</td></tr><tr><td class="event"><a href="http://tcx3.travian.com/spieler.php?uid=708">Zmija</a> has joined the alliance.</td><td class="dat">29.08.11 00:43</td></tr></tbody></table></div>

<div id="side_info">

	<table id="vlist" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<td colspan="3">
	<img src="allianznews_files/x.gif" class="tSwitch dynamic_img opened" alt="">&nbsp;<a href="http://tcx3.travian.com/dorf3.php" accesskey="9">Villages</a></td>
		</tr>
	</thead>
	<tbody>
	<tr><td class="dot hl">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=28790&amp;s=4">01.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-2</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=11112&amp;s=4" accesskey="n">02.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(0</div>
			<div class="pi">|</div>
			<div class="coy">90)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=7346&amp;s=4">03.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(1</div>
			<div class="pi">|</div>
			<div class="coy">73)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=37728&amp;s=4">04.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-1</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=47024&amp;s=4">05.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-1</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=61009&amp;s=4">06.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-5</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=91575&amp;s=4">07.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-2</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=90707&amp;s=4">08.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">170)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=80543&amp;s=4">09.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">169)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=56287&amp;s=4">10.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(71</div>
			<div class="pi">|</div>
			<div class="coy">171)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=72152&amp;s=4">11.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">173)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=64496&amp;s=4">12.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-34</div>
			<div class="pi">|</div>
			<div class="coy">124)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=9007&amp;s=4">13.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-14</div>
			<div class="pi">|</div>
			<div class="coy">79)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=96539&amp;s=4">14.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-3</div>
			<div class="pi">|</div>
			<div class="coy">85)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=99425&amp;s=4" accesskey="l">15.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-3</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr></tbody>
</table></div>
<div class="clear"></div>
</div>


<div class="footer-stopper"></div>
<div class="clear"></div>
<div id="footer">
	<div id="mfoot">
		<div class="footer-menu">
			<a href="http://www.travian.com/#screenshots">Screenshots</a> | 
<a href="http://www.travian.com/#spielregeln">Game rules</a> | 
<a href="http://www.travian.com/#agb">Terms</a> | 
<a href="http://www.travian.com/#impressum">Imprint</a>
			<br>
			<div class="copyright">© 2004 - 2011 Travian Games GmbH</div>
							</div>
			</div>
    <div id="cfoot"></div>
</div>

<div id="res">
	<div id="resWrap">
		<table cellpadding="1" cellspacing="1">
			<tbody><tr>
									<td><img src="allianznews_files/x.gif" class="r1" alt="Lumber" title="Lumber"></td>
					<td id="l4" title="1857">158488/261400</td>
									<td><img src="allianznews_files/x.gif" class="r2" alt="Clay" title="Clay"></td>
					<td id="l3" title="1485">162446/261400</td>
									<td><img src="allianznews_files/x.gif" class="r3" alt="Iron" title="Iron"></td>
					<td id="l2" title="1485">46401/261400</td>
									<td><img src="allianznews_files/x.gif" class="r4" alt="Crop" title="Crop"></td>
					<td id="l1" title="-13969">78101/215100</td>
													</tr>
			<tr>
				<td colspan="6"></td>
							<td><img src="allianznews_files/x.gif" class="r5" alt="Crop consumption" title="Crop consumption"></td>
				<td>61891/47922</td>
			</tr>
		</tbody></table>
	</div>
</div>



<div id="stime">
	<div id="ltime">
		<div id="ltimeWrap">
						Calculated in <b>41</b> ms

						<br>Server time: <span id="tp1" class="b">16:25:27</span>

			
			
					</div>
	</div>
</div>

<div id="ce"></div>
</div>


</body></html>