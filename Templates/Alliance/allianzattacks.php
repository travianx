<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head>
	<title>Travian comx</title>
	<meta http-equiv="cache-control" content="max-age=0">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="imagetoolbar" content="no">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<script src="allianzattacks_files/mt-full.js" type="text/javascript"></script>
	<script src="allianzattacks_files/unx.js" type="text/javascript"></script>
	<script src="allianzattacks_files/new.js" type="text/javascript"></script>
	        <link href="allianzattacks_files/compact.css" rel="stylesheet" type="text/css"> 
<link href="allianzattacks_files/lang_002.css" rel="stylesheet" type="text/css"> 
<link href="allianzattacks_files/travian.css" rel="stylesheet" type="text/css"> 
<link href="allianzattacks_files/lang.css" rel="stylesheet" type="text/css"> 
        	<script type="text/javascript">
		window.addEvent('domready', start);
	</script>
</head>


<body class="v35 gecko">


<div class="wrapper">
<img src="allianzattacks_files/x.gif" id="msfilter" alt="">
<div id="dynamic_header">
	</div>


<div id="header">
	<div id="mtop">
		<a href="http://tcx3.travian.com/dorf1.php" id="n1" accesskey="1"><img src="allianzattacks_files/x.gif" title="Village overview" alt="Village overview"></a>
		<a href="http://tcx3.travian.com/dorf2.php" id="n2" accesskey="2"><img src="allianzattacks_files/x.gif" title="Village centre" alt="Village centre"></a>
		<a href="http://tcx3.travian.com/karte.php" id="n3" accesskey="3"><img src="allianzattacks_files/x.gif" title="Map" alt="Map"></a>
		<a href="http://tcx3.travian.com/statistiken.php" id="n4" accesskey="4"><img src="allianzattacks_files/x.gif" title="Statistics" alt="Statistics"></a>
  		<div id="n5" class="i3">
			<a class="reports" href="http://tcx3.travian.com/berichte.php" accesskey="5"><img src="allianzattacks_files/x.gif" class="l" title="Reports" alt="Reports"></a>
			<a class="messages" href="http://tcx3.travian.com/nachrichten.php" accesskey="6"><img src="allianzattacks_files/x.gif" class="r" title="Messages" alt="Messages"></a>
		</div>
		<a href="http://tcx3.travian.com/plus.php" id="plus"><span class="plus_text"><span class="plus_g">P</span><span class="plus_o">l</span><span class="plus_g">u</span><span class="plus_o">s</span></span><img src="allianzattacks_files/x.gif" id="btn_plus" class="inactive" title="Plus menu" alt="Plus menu"></a>
		<div class="clear"></div>
	</div>
</div>

<div id="mid">
		<div id="side_navi">
		<a id="logo" href="http://www.travian.com/"><img src="allianzattacks_files/x.gif" alt="Travian"></a>
			<p>
			<a href="http://www.travian.com/">Homepage</a>
            <a href="#" onclick="return Popup(0,0);">Instructions</a>
			<a href="http://tcx3.travian.com/spieler.php?uid=7942">Profile</a>
			<a href="http://tcx3.travian.com/logout.php">Logout</a>
		</p>
		<p>
							<a href="http://forum.travian.com/" target="_blank">Forum</a>
						
					</p>
		<p>
			<a href="http://tcx3.travian.com/plus.php?id=3">Travian <b><span class="plus_g">P</span><span class="plus_o">l</span><span class="plus_g">u</span><span class="plus_o">s</span></b></a>
			<a href="http://tcx3.travian.com/support.php">Support</a>
		</p>
							</div>

	<div id="content" class="alliance">
<h1>LiQ-G - LiQuiDaTion-Green Team</h1>
<div id="textmenu">
   <a href="http://tcx3.travian.com/allianz.php">Overview</a>
 | <a href="http://tcx3.travian.com/allianz.php?s=2">Forum</a>
 | <a href="http://tcx3.travian.com/allianz.php?s=6">Chat</a>
 | <a href="http://tcx3.travian.com/allianz.php?s=3" class="selected ">Attacks</a>
 | <a href="http://tcx3.travian.com/allianz.php?s=4">News</a>
 | <a href="http://tcx3.travian.com/allianz.php?s=5">Options</a>
</div>
<table id="offs" cellpadding="1" cellspacing="1"><thead>
<tr>
	<th colspan="4">
		Military events
		<div id="submenu">
			<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=32">
				<img src="allianzattacks_files/x.gif" class="btn_def" alt="Defender" title="Defender">
			</a>
			<a href="http://tcx3.travian.com/allianz.php?s=3">
				<img src="allianzattacks_files/x.gif" class="btn_off active" alt="Attacker" title="Attacker">
			</a>
		</div>
	</th>
</tr>
<tr>
<td>Player</td>
<td>Alliance</td>
<td>Date</td>
</tr></thead><tbody>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62109459">Ares C attacks Raider</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:24</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62109246">levantan attacks paavoli</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:22</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62109186">levantan attacks LeafDude</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:21</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108935">stanly attacks SLK</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:19</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108924">Ares C attacks Ares C</a></div>
	</td>
	<td class="al"><a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G
		</a></td>
	<td class="dat">today 16:19</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108826">Ares C attacks Hellscream</a></div>
	</td>
	<td class="al"><a href="http://tcx3.travian.com/allianz.php?aid=4043">Warcraft
		</a></td>
	<td class="dat">today 16:18</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108787">Ares C attacks CRACKWILD</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:17</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108744">Ares C attacks kualabera</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:17</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=2">
			<img src="allianzattacks_files/x.gif" class="iReport iReport2" alt="Won as attacker with losses." title="Won as attacker with losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108540">Piush attacks Endymion</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:15</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108413">stanly attacks stanly</a></div>
	</td>
	<td class="al"><a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G
		</a></td>
	<td class="dat">today 16:14</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=2">
			<img src="allianzattacks_files/x.gif" class="iReport iReport2" alt="Won as attacker with losses." title="Won as attacker with losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108374">levantan attacks Eitah</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:13</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=2">
			<img src="allianzattacks_files/x.gif" class="iReport iReport2" alt="Won as attacker with losses." title="Won as attacker with losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108230">levantan attacks Nature Fighter</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:12</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108223">levantan attacks Cardoctorr</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:12</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108213">levantan attacks robrich125</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:12</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108170">Ares C attacks black water</a></div>
	</td>
	<td class="al"><a href="http://tcx3.travian.com/allianz.php?aid=2155">Otelo
		</a></td>
	<td class="dat">today 16:12</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108165">Ares C attacks BillabongBoy</a></div>
	</td>
	<td class="al"><a href="http://tcx3.travian.com/allianz.php?aid=1784">$M$
		</a></td>
	<td class="dat">today 16:12</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108134">dariush attacks vaqsu</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:11</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108093">levantan attacks robrich125</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:11</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62108068">Ares C attacks wiselion</a></div>
	</td>
	<td class="al"><a href="http://tcx3.travian.com/allianz.php?aid=1865">EPIC
		</a></td>
	<td class="dat">today 16:11</td></tr>
	<tr><td class="sub">
		<a href="http://tcx3.travian.com/allianz.php?s=3&amp;f=1">
			<img src="allianzattacks_files/x.gif" class="iReport iReport1" alt="Won as attacker without losses." title="Won as attacker without losses.">
		</a>
		<div><a href="http://tcx3.travian.com/berichte.php?id=62107984">Ares C attacks birdfever2</a></div>
	</td>
	<td class="al">-</td>
	<td class="dat">today 16:10</td></tr></tbody></table></div>

<div id="side_info">

	<table id="vlist" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<td colspan="3">
	<img src="allianzattacks_files/x.gif" class="tSwitch dynamic_img opened" alt="">&nbsp;<a href="http://tcx3.travian.com/dorf3.php" accesskey="9">Villages</a></td>
		</tr>
	</thead>
	<tbody>
	<tr><td class="dot hl">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=28790&amp;s=3">01.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-2</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=11112&amp;s=3" accesskey="n">02.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(0</div>
			<div class="pi">|</div>
			<div class="coy">90)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=7346&amp;s=3">03.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(1</div>
			<div class="pi">|</div>
			<div class="coy">73)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=37728&amp;s=3">04.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-1</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=47024&amp;s=3">05.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-1</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=61009&amp;s=3">06.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-5</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=91575&amp;s=3">07.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-2</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=90707&amp;s=3">08.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">170)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=80543&amp;s=3">09.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">169)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=56287&amp;s=3">10.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(71</div>
			<div class="pi">|</div>
			<div class="coy">171)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=72152&amp;s=3">11.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">173)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=64496&amp;s=3">12.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-34</div>
			<div class="pi">|</div>
			<div class="coy">124)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=9007&amp;s=3">13.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-14</div>
			<div class="pi">|</div>
			<div class="coy">79)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=96539&amp;s=3">14.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-3</div>
			<div class="pi">|</div>
			<div class="coy">85)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/allianz.php?newdid=99425&amp;s=3" accesskey="l">15.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-3</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr></tbody>
</table></div>
<div class="clear"></div>
</div>


<div class="footer-stopper"></div>
<div class="clear"></div>
<div id="footer">
	<div id="mfoot">
		<div class="footer-menu">
			<a href="http://www.travian.com/#screenshots">Screenshots</a> | 
<a href="http://www.travian.com/#spielregeln">Game rules</a> | 
<a href="http://www.travian.com/#agb">Terms</a> | 
<a href="http://www.travian.com/#impressum">Imprint</a>
			<br>
			<div class="copyright">© 2004 - 2011 Travian Games GmbH</div>
							</div>
			</div>
    <div id="cfoot"></div>
</div>

<div id="res">
	<div id="resWrap">
		<table cellpadding="1" cellspacing="1">
			<tbody><tr>
									<td><img src="allianzattacks_files/x.gif" class="r1" alt="Lumber" title="Lumber"></td>
					<td id="l4" title="1857">158524/261400</td>
									<td><img src="allianzattacks_files/x.gif" class="r2" alt="Clay" title="Clay"></td>
					<td id="l3" title="1485">162474/261400</td>
									<td><img src="allianzattacks_files/x.gif" class="r3" alt="Iron" title="Iron"></td>
					<td id="l2" title="1485">46429/261400</td>
									<td><img src="allianzattacks_files/x.gif" class="r4" alt="Crop" title="Crop"></td>
					<td id="l1" title="-13969">77831/215100</td>
													</tr>
			<tr>
				<td colspan="6"></td>
							<td><img src="allianzattacks_files/x.gif" class="r5" alt="Crop consumption" title="Crop consumption"></td>
				<td>61891/47922</td>
			</tr>
		</tbody></table>
	</div>
</div>



<div id="stime">
	<div id="ltime">
		<div id="ltimeWrap">
						Calculated in <b>208</b> ms

						<br>Server time: <span id="tp1" class="b">16:26:36</span>

			
			
					</div>
	</div>
</div>

<div id="ce"></div>
</div>


</body></html>