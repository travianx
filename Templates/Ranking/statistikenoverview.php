<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head>
	<title>Travian comx</title>
	<meta http-equiv="cache-control" content="max-age=0">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="imagetoolbar" content="no">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<script src="statistikenoverview_files/mt-full.js" type="text/javascript"></script>
	<script src="statistikenoverview_files/unx.js" type="text/javascript"></script>
	<script src="statistikenoverview_files/new.js" type="text/javascript"></script>
	        <link href="statistikenoverview_files/compact.css" rel="stylesheet" type="text/css"> 
<link href="statistikenoverview_files/lang_002.css" rel="stylesheet" type="text/css"> 
<link href="statistikenoverview_files/travian.css" rel="stylesheet" type="text/css"> 
<link href="statistikenoverview_files/lang.css" rel="stylesheet" type="text/css"> 
        	<script type="text/javascript">
		window.addEvent('domready', start);
	</script>
</head>


<body class="v35 gecko">


<div class="wrapper">
<img src="statistikenoverview_files/x.gif" id="msfilter" alt="">
<div id="dynamic_header">
	</div>


<div id="header">
	<div id="mtop">
		<a href="http://tcx3.travian.com/dorf1.php" id="n1" accesskey="1"><img src="statistikenoverview_files/x.gif" title="Village overview" alt="Village overview"></a>
		<a href="http://tcx3.travian.com/dorf2.php" id="n2" accesskey="2"><img src="statistikenoverview_files/x.gif" title="Village centre" alt="Village centre"></a>
		<a href="http://tcx3.travian.com/karte.php" id="n3" accesskey="3"><img src="statistikenoverview_files/x.gif" title="Map" alt="Map"></a>
		<a href="http://tcx3.travian.com/statistiken.php" id="n4" accesskey="4"><img src="statistikenoverview_files/x.gif" title="Statistics" alt="Statistics"></a>
  		<div id="n5" class="i3">
			<a class="reports" href="http://tcx3.travian.com/berichte.php" accesskey="5"><img src="statistikenoverview_files/x.gif" class="l" title="Reports" alt="Reports"></a>
			<a class="messages" href="http://tcx3.travian.com/nachrichten.php" accesskey="6"><img src="statistikenoverview_files/x.gif" class="r" title="Messages" alt="Messages"></a>
		</div>
		<a href="http://tcx3.travian.com/plus.php" id="plus"><span class="plus_text"><span class="plus_g">P</span><span class="plus_o">l</span><span class="plus_g">u</span><span class="plus_o">s</span></span><img src="statistikenoverview_files/x.gif" id="btn_plus" class="inactive" title="Plus menu" alt="Plus menu"></a>
		<div class="clear"></div>
	</div>
</div>

<div id="mid">
		<div id="side_navi">
		<a id="logo" href="http://www.travian.com/"><img src="statistikenoverview_files/x.gif" alt="Travian"></a>
			<p>
			<a href="http://www.travian.com/">Homepage</a>
            <a href="#" onclick="return Popup(0,0);">Instructions</a>
			<a href="http://tcx3.travian.com/spieler.php?uid=7942">Profile</a>
			<a href="http://tcx3.travian.com/logout.php">Logout</a>
		</p>
		<p>
							<a href="http://forum.travian.com/" target="_blank">Forum</a>
						
					</p>
		<p>
			<a href="http://tcx3.travian.com/plus.php?id=3">Travian <b><span class="plus_g">P</span><span class="plus_o">l</span><span class="plus_g">u</span><span class="plus_o">s</span></b></a>
			<a href="http://tcx3.travian.com/support.php">Support</a>
		</p>
							</div>

	<div id="content" class="statistics">
<h1>Statistics</h1>

<div id="textmenu">
   <a href="http://tcx3.travian.com/statistiken.php" class="selected ">Player</a>
 | <a href="http://tcx3.travian.com/statistiken.php?id=4">Alliances</a>
 | <a href="http://tcx3.travian.com/statistiken.php?id=2">Villages</a>
 | <a href="http://tcx3.travian.com/statistiken.php?id=8">Heroes</a>
 | <a href="http://tcx3.travian.com/statistiken.php?id=0">General</a>
 | <a href="http://tcx3.travian.com/statistiken.php?id=6">WW</a>
</div>
		<table id="player" class="row_table_data" cellpadding="1" cellspacing="1">
			<thead>
				<tr>
					<th colspan="5">
						The largest players						<div id="submenu"><a title="Top 10" href="http://tcx3.travian.com/statistiken.php?id=7"><img class="btn_top10" src="statistikenoverview_files/x.gif" alt="Top 10"></a><a title="defender" href="http://tcx3.travian.com/statistiken.php?id=32"><img class="btn_def" src="statistikenoverview_files/x.gif" alt="defender"></a><a title="attacker" href="http://tcx3.travian.com/statistiken.php?id=31"><img class="btn_off" src="statistikenoverview_files/x.gif" alt="attacker"></a></div><div id="submenu2"><a title="Romans" href="http://tcx3.travian.com/statistiken.php?id=11"><img class="btn_v1" src="statistikenoverview_files/x.gif" alt="attacker"></a><a title="Teutons" href="http://tcx3.travian.com/statistiken.php?id=12"><img class="btn_v2" src="statistikenoverview_files/x.gif" alt="attacker"></a><a title="Gauls" href="http://tcx3.travian.com/statistiken.php?id=13"><img class="btn_v3" src="statistikenoverview_files/x.gif" alt="attacker"></a></div>					</th>
				</tr>
		<tr><td></td><td>Player</td><td>Alliance</td><td>Population</td><td>Villages</td></tr>
		</thead><tbody>  <tr>
   <td class="ra ">501.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=7054">Svarog</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=1168">LiQ-H</a></td>
   <td class="pop ">8859</td>
   <td class="vil ">18</td>
  </tr>
  <tr class="">
   <td class="ra ">502.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=2523">Black ExS</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a></td>
   <td class="pop ">8856</td>
   <td class="vil ">12</td>
  </tr>
  <tr>
   <td class="ra ">503.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=7275">ratz</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=50">Dacia</a></td>
   <td class="pop ">8854</td>
   <td class="vil ">13</td>
  </tr>
  <tr>
   <td class="ra ">504.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=1578">GypoStu</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a></td>
   <td class="pop ">8840</td>
   <td class="vil ">13</td>
  </tr>
  <tr>
   <td class="ra ">505.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=5340">LT-GOD</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=50">Dacia</a></td>
   <td class="pop ">8838</td>
   <td class="vil ">21</td>
  </tr>
  <tr class="hl">
   <td class="ra  fc">506.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=7942">kailoop</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=185">LiQ-G</a></td>
   <td class="pop ">8834</td>
   <td class="vil  lc">15</td>
  </tr>
  <tr class="">
   <td class="ra ">507.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=9349">Mik</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=61">LIQ-A</a></td>
   <td class="pop ">8785</td>
   <td class="vil ">17</td>
  </tr>
  <tr class="">
   <td class="ra ">508.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=395">romulus</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=4">CHAOS™</a></td>
   <td class="pop ">8765</td>
   <td class="vil ">12</td>
  </tr>
  <tr class="">
   <td class="ra ">509.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=17288">darkxx</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=23">TLL™</a></td>
   <td class="pop ">8755</td>
   <td class="vil ">15</td>
  </tr>
  <tr>
   <td class="ra ">510.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=138">lazizai</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=49">MYTH</a></td>
   <td class="pop ">8750</td>
   <td class="vil ">14</td>
  </tr>
  <tr class="">
   <td class="ra ">511.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=229">killa_haze</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=4">CHAOS™</a></td>
   <td class="pop ">8743</td>
   <td class="vil ">18</td>
  </tr>
  <tr>
   <td class="ra ">512.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=5125">2OlteniBetzivi</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=4">CHAOS™</a></td>
   <td class="pop ">8732</td>
   <td class="vil ">15</td>
  </tr>
  <tr>
   <td class="ra ">513.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=3420">Chief</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=1168">LiQ-H</a></td>
   <td class="pop ">8719</td>
   <td class="vil ">14</td>
  </tr>
  <tr>
   <td class="ra ">514.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=275">tosic</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=49">MYTH</a></td>
   <td class="pop ">8711</td>
   <td class="vil ">15</td>
  </tr>
  <tr>
   <td class="ra ">515.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=11546">videorpg</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=1168">LiQ-H</a></td>
   <td class="pop ">8670</td>
   <td class="vil ">16</td>
  </tr>
  <tr>
   <td class="ra ">516.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=6359">mark761</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=152">LIQ-IM</a></td>
   <td class="pop ">8648</td>
   <td class="vil ">17</td>
  </tr>
  <tr>
   <td class="ra ">517.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=20729">Chevelle</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=80">ABC-D.B.</a></td>
   <td class="pop ">8644</td>
   <td class="vil ">15</td>
  </tr>
  <tr>
   <td class="ra ">518.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=2036">Knocks</a> </td>
   <td class="al ">-</td>
   <td class="pop ">8623</td>
   <td class="vil ">14</td>
  </tr>
  <tr>
   <td class="ra ">519.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=1280">Mirana</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=175">LiQ-Y</a></td>
   <td class="pop ">8623</td>
   <td class="vil ">15</td>
  </tr>
  <tr>
   <td class="ra ">520.</td>
   <td class="pla "><a href="http://tcx3.travian.com/spieler.php?uid=25678">rastik92</a> </td>
   <td class="al "><a href="http://tcx3.travian.com/allianz.php?aid=80">ABC-D.B.</a></td>
   <td class="pop ">8613</td>
   <td class="vil ">15</td>
  </tr>
 </tbody>
</table>
<table id="search_navi" cellpadding="1" cellspacing="1">
					<tbody><tr>
						<td>
							<form method="post" action="statistiken.php?id=1">
							<div class="search">
								<span>rank
								<input name="timestamp" value="1315322553" type="hidden">
								<input name="timestamp_checksum" value="f0f3b3" type="hidden">
								<input class="text ra" maxlength="5" name="rank" value="506" type="text"></span>
								<span class="or">or</span>
								<span>name<input class="text name" maxlength="20" name="name" type="text"></span>
								<input value="submit" name="submit" id="btn_ok" class="dynamic_img" src="statistikenoverview_files/x.gif" alt="OK" type="image">
							</div>
							</form>
							<div class="navi"><a href="http://tcx3.travian.com/statistiken.php?id=1&amp;rank=481&amp;c=e2fc7f#h2">« back</a>|<a href="http://tcx3.travian.com/statistiken.php?id=1&amp;rank=521&amp;c=d8b522#h2">forward »</a></div>
						</td>
					</tr>
				</tbody></table></div>

<div id="side_info">

	<table id="vlist" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<td colspan="3">
	<img src="statistikenoverview_files/x.gif" class="tSwitch dynamic_img opened" alt="">&nbsp;<a href="http://tcx3.travian.com/dorf3.php" accesskey="9">Villages</a></td>
		</tr>
	</thead>
	<tbody>
	<tr><td class="dot hl">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=28790&amp;id=1">01.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-2</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=11112&amp;id=1" accesskey="n">02.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(0</div>
			<div class="pi">|</div>
			<div class="coy">90)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=7346&amp;id=1">03.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(1</div>
			<div class="pi">|</div>
			<div class="coy">73)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=37728&amp;id=1">04.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-1</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=47024&amp;id=1">05.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-1</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=61009&amp;id=1">06.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-5</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=91575&amp;id=1">07.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-2</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=90707&amp;id=1">08.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">170)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=80543&amp;id=1">09.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">169)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=56287&amp;id=1">10.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(71</div>
			<div class="pi">|</div>
			<div class="coy">171)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=72152&amp;id=1">11.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">173)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=64496&amp;id=1">12.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-34</div>
			<div class="pi">|</div>
			<div class="coy">124)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=9007&amp;id=1">13.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-14</div>
			<div class="pi">|</div>
			<div class="coy">79)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=96539&amp;id=1">14.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-3</div>
			<div class="pi">|</div>
			<div class="coy">85)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/statistiken.php?newdid=99425&amp;id=1" accesskey="l">15.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-3</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr></tbody>
</table></div>
<div class="clear"></div>
</div>


<div class="footer-stopper"></div>
<div class="clear"></div>
<div id="footer">
	<div id="mfoot">
		<div class="footer-menu">
			<a href="http://www.travian.com/#screenshots">Screenshots</a> | 
<a href="http://www.travian.com/#spielregeln">Game rules</a> | 
<a href="http://www.travian.com/#agb">Terms</a> | 
<a href="http://www.travian.com/#impressum">Imprint</a>
			<br>
			<div class="copyright">© 2004 - 2011 Travian Games GmbH</div>
							</div>
			</div>
    <div id="cfoot"></div>
</div>

<div id="res">
	<div id="resWrap">
		<table cellpadding="1" cellspacing="1">
			<tbody><tr>
									<td><img src="statistikenoverview_files/x.gif" class="r1" alt="Lumber" title="Lumber"></td>
					<td id="l4" title="1857">158420/261400</td>
									<td><img src="statistikenoverview_files/x.gif" class="r2" alt="Clay" title="Clay"></td>
					<td id="l3" title="1485">162391/261400</td>
									<td><img src="statistikenoverview_files/x.gif" class="r3" alt="Iron" title="Iron"></td>
					<td id="l2" title="1485">46346/261400</td>
									<td><img src="statistikenoverview_files/x.gif" class="r4" alt="Crop" title="Crop"></td>
					<td id="l1" title="-13967">78615/215100</td>
													</tr>
			<tr>
				<td colspan="6"></td>
							<td><img src="statistikenoverview_files/x.gif" class="r5" alt="Crop consumption" title="Crop consumption"></td>
				<td>61889/47922</td>
			</tr>
		</tbody></table>
	</div>
</div>



<div id="stime">
	<div id="ltime">
		<div id="ltimeWrap">
						Calculated in <b>31</b> ms

						<br>Server time: <span id="tp1" class="b">16:23:14</span>

			
			
					</div>
	</div>
</div>

<div id="ce"></div>
</div>


</body></html>