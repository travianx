<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head>
	<title>Travian comx</title>
	<meta http-equiv="cache-control" content="max-age=0">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="imagetoolbar" content="no">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<script src="buildheroland_files/mt-full.js" type="text/javascript"></script>
	<script src="buildheroland_files/unx.js" type="text/javascript"></script>
	<script src="buildheroland_files/new.js" type="text/javascript"></script>
	        <link href="buildheroland_files/compact.css" rel="stylesheet" type="text/css"> 
<link href="buildheroland_files/lang_002.css" rel="stylesheet" type="text/css"> 
<link href="buildheroland_files/travian.css" rel="stylesheet" type="text/css"> 
<link href="buildheroland_files/lang.css" rel="stylesheet" type="text/css"> 
        	<script type="text/javascript">
		window.addEvent('domready', start);
	</script>
</head>


<body class="v35 gecko">


<div class="wrapper">
<img src="buildheroland_files/x.gif" id="msfilter" alt="">
<div id="dynamic_header">
	</div>


<div id="header">
	<div id="mtop">
		<a href="http://tcx3.travian.com/dorf1.php" id="n1" accesskey="1"><img src="buildheroland_files/x.gif" title="Village overview" alt="Village overview"></a>
		<a href="http://tcx3.travian.com/dorf2.php" id="n2" accesskey="2"><img src="buildheroland_files/x.gif" title="Village centre" alt="Village centre"></a>
		<a href="http://tcx3.travian.com/karte.php" id="n3" accesskey="3"><img src="buildheroland_files/x.gif" title="Map" alt="Map"></a>
		<a href="http://tcx3.travian.com/statistiken.php" id="n4" accesskey="4"><img src="buildheroland_files/x.gif" title="Statistics" alt="Statistics"></a>
  		<div id="n5" class="i3">
			<a class="reports" href="http://tcx3.travian.com/berichte.php" accesskey="5"><img src="buildheroland_files/x.gif" class="l" title="Reports" alt="Reports"></a>
			<a class="messages" href="http://tcx3.travian.com/nachrichten.php" accesskey="6"><img src="buildheroland_files/x.gif" class="r" title="Messages" alt="Messages"></a>
		</div>
		<a href="http://tcx3.travian.com/plus.php" id="plus"><span class="plus_text"><span class="plus_g">P</span><span class="plus_o">l</span><span class="plus_g">u</span><span class="plus_o">s</span></span><img src="buildheroland_files/x.gif" id="btn_plus" class="inactive" title="Plus menu" alt="Plus menu"></a>
		<div class="clear"></div>
	</div>
</div>

<div id="mid">
		<div id="side_navi">
		<a id="logo" href="http://www.travian.com/"><img src="buildheroland_files/x.gif" alt="Travian"></a>
			<p>
			<a href="http://www.travian.com/">Homepage</a>
            <a href="#" onclick="return Popup(0,0);">Instructions</a>
			<a href="http://tcx3.travian.com/spieler.php?uid=7942">Profile</a>
			<a href="http://tcx3.travian.com/logout.php">Logout</a>
		</p>
		<p>
							<a href="http://forum.travian.com/" target="_blank">Forum</a>
						
					</p>
		<p>
			<a href="http://tcx3.travian.com/plus.php?id=3">Travian <b><span class="plus_g">P</span><span class="plus_o">l</span><span class="plus_g">u</span><span class="plus_o">s</span></b></a>
			<a href="http://tcx3.travian.com/support.php">Support</a>
		</p>
							</div>

	<div id="content" class="build">
<div id="build" class="gid37"><h1>Hero's Mansion <span class="level">Level 10</span></h1>
<p class="build_desc">
	<a href="#" onclick="return Popup(37,4, 'gid');" class="build_logo"> <img class="building g37" src="buildheroland_files/x.gif" alt="Hero's Mansion" title="Hero's Mansion"> </a>
	In the Hero's Mansion you can train a hero and starting with building level 10 occupy oases in your surrounding area.</p>

<table id="oases" cellpadding="1" cellspacing="1">
	<thead><tr>
		<th colspan="4">oases</th>
	</tr>
	<tr>
		<td>Name</td>
		<td>Coordinates</td>
		<td>Loyalty</td>
		<td>Resources</td>
	</tr></thead>
	<tbody><tr>
			<td class="nam">
				<a href="http://tcx3.travian.com/build.php?a=37&amp;gid=37&amp;c=c94&amp;del=21345&amp;land"><img class="del" src="buildheroland_files/x.gif" alt="delete" title="delete"></a>
				<a href="http://tcx3.travian.com/karte.php?d=256718&amp;c=41">Occupied oasis</a>
			</td>
			<td class="aligned_coords">
				<div class="cox">(-3</div>
				<div class="pi">|</div>
				<div class="coy">80)</div>
			</td>
			<td class="zp">100%</td>
			<td class="res"> <img class="r1" src="buildheroland_files/x.gif" alt="Lumber" title="Lumber">+25% <img class="r4" src="buildheroland_files/x.gif" alt="Crop" title="Crop">+25%</td></tr></tbody></table></div></div>

<div id="side_info">

	<table id="vlist" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<td colspan="3">
	<img src="buildheroland_files/x.gif" class="tSwitch dynamic_img opened" alt="">&nbsp;<a href="http://tcx3.travian.com/dorf3.php" accesskey="9">Villages</a></td>
		</tr>
	</thead>
	<tbody>
	<tr><td class="dot hl">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=28790&amp;gid=37&amp;id=31">01.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-2</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=11112&amp;gid=37&amp;id=31" accesskey="n">02.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(0</div>
			<div class="pi">|</div>
			<div class="coy">90)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=7346&amp;gid=37&amp;id=31">03.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(1</div>
			<div class="pi">|</div>
			<div class="coy">73)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=37728&amp;gid=37&amp;id=31">04.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-1</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=47024&amp;gid=37&amp;id=31">05.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-1</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=61009&amp;gid=37&amp;id=31">06.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-5</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=91575&amp;gid=37&amp;id=31">07.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-2</div>
			<div class="pi">|</div>
			<div class="coy">84)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=90707&amp;gid=37&amp;id=31">08.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">170)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=80543&amp;gid=37&amp;id=31">09.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">169)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=56287&amp;gid=37&amp;id=31">10.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(71</div>
			<div class="pi">|</div>
			<div class="coy">171)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=72152&amp;gid=37&amp;id=31">11.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(69</div>
			<div class="pi">|</div>
			<div class="coy">173)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=64496&amp;gid=37&amp;id=31">12.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-34</div>
			<div class="pi">|</div>
			<div class="coy">124)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=9007&amp;gid=37&amp;id=31">13.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-14</div>
			<div class="pi">|</div>
			<div class="coy">79)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=96539&amp;gid=37&amp;id=31">14.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-3</div>
			<div class="pi">|</div>
			<div class="coy">85)</div>
			</td>
		</tr><tr><td class="dot">●</td>
			<td class="link"><div><a href="http://tcx3.travian.com/build.php?newdid=99425&amp;gid=37&amp;id=31" accesskey="l">15.XIPH</a></div></td><td class="aligned_coords">
			<div class="cox">(-3</div>
			<div class="pi">|</div>
			<div class="coy">83)</div>
			</td>
		</tr></tbody>
</table></div>
<div class="clear"></div>
</div>


<div class="footer-stopper"></div>
<div class="clear"></div>
<div id="footer">
	<div id="mfoot">
		<div class="footer-menu">
			<a href="http://www.travian.com/#screenshots">Screenshots</a> | 
<a href="http://www.travian.com/#spielregeln">Game rules</a> | 
<a href="http://www.travian.com/#agb">Terms</a> | 
<a href="http://www.travian.com/#impressum">Imprint</a>
			<br>
			<div class="copyright">© 2004 - 2011 Travian Games GmbH</div>
							</div>
			</div>
    <div id="cfoot"></div>
</div>

<div id="res">
	<div id="resWrap">
		<table cellpadding="1" cellspacing="1">
			<tbody><tr>
									<td><img src="buildheroland_files/x.gif" class="r1" alt="Lumber" title="Lumber"></td>
					<td id="l4" title="1857">158363/261400</td>
									<td><img src="buildheroland_files/x.gif" class="r2" alt="Clay" title="Clay"></td>
					<td id="l3" title="1485">162346/261400</td>
									<td><img src="buildheroland_files/x.gif" class="r3" alt="Iron" title="Iron"></td>
					<td id="l2" title="1485">46301/261400</td>
									<td><img src="buildheroland_files/x.gif" class="r4" alt="Crop" title="Crop"></td>
					<td id="l1" title="-13966">79043/215100</td>
													</tr>
			<tr>
				<td colspan="6"></td>
							<td><img src="buildheroland_files/x.gif" class="r5" alt="Crop consumption" title="Crop consumption"></td>
				<td>61888/47922</td>
			</tr>
		</tbody></table>
	</div>
</div>



<div id="stime">
	<div id="ltime">
		<div id="ltimeWrap">
						Calculated in <b>25</b> ms

						<br>Server time: <span id="tp1" class="b">16:21:25</span>

			
			
					</div>
	</div>
</div>

<div id="ce"></div>
</div>


</body></html>